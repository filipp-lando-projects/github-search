import './App.css';
import React from "react";
import Search from "./components/Search";
import Container from "react-bootstrap/Container";
import * as requests from "./api/api"
import {getPrsAndIssues} from "./api/api"
import RepositoryBlock from "./components/RepositoryBlock";
import Pagination from "./components/Pagination";
import moment from "moment";
import * as _ from 'lodash'
import PreviousSearches from "./components/PreviousSearches";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.multiselectRef = React.createRef();
        this.state = {
            minStars: "",
            maxStars: "",
            dateFrom: moment.unix(0),
            dateTo: moment(),
            options: [],
            repositories: [],
            lastPage: 10,
            items: {},
            prevSearches: []
        }
    }

    componentDidMount() {
        requests.getLanguagesList()
            .then(data => {
                const languages_options = data.map((el, i) => {
                    return {name: el, id: i}
                })
                this.setState({
                    options: languages_options
                })
            })
    }

    handleInputChange = (event, name) => {
        const target = event.target;
        let value = target.value;
        if (name === "maxStars" || name === "minStars") {
            if (value < 0) {
                value = value * (-1)
            }
        }
        this.setState({
            [name]: value
        });
    }

    getSelectedLanguages = () => {
        return this.multiselectRef.current.getSelectedItems()
    }

    onSubmitSearch = (submitRequest = null, pageNumber = 1) => {
        const languages = this.getSelectedLanguages();
        let request
        if (submitRequest) {
            request = submitRequest
        } else {
            request = {
                date_from: this.state.dateFrom.format("YYYY-MM-DD"),
                date_to: this.state.dateTo.format("YYYY-MM-DD"),
                page_number: pageNumber,
                programming_languages: languages.map(el => {
                    return el.name
                })
            }
            if (this.state.minStars) {
                request['min_stars'] = parseInt(this.state.minStars)
            }
            if (this.state.maxStars) {
                request['max_stars'] = parseInt(this.state.maxStars)
            }
        }
        requests.getRepositories(request)
            .then(data => {
                    this.setState({
                        repositories: data.repositories,
                        lastPage: data.last_page
                    })
                    this.updatePreviousSearches(request)
                }
            )
    }

    requestDetails = async (user, repository) => {
        if (repository in this.state.items) {
            return this.state.items[repository];
        } else {
            getPrsAndIssues({user: user, repository: repository}).then(response => {
                items[repository] = {
                    total_count_prs: response.total_count_prs,
                    total_count_open_issues: response.total_count_open_issues,
                    total_counst_closed_issues: response.total_count_closed_issues
                }
            })
        }

    }

    updatePreviousSearches = (search) => {
        if (!_.isEqual(search, this.state.prevSearches[this.state.prevSearches.length - 1])) {
            this.setState(prevState => {
                    if (prevState.prevSearches.length >= 5) {
                        return {
                            prevSearches: [...prevState.prevSearches.slice(1), search]
                        }
                    } else {
                        return {
                            prevSearches: [...prevState.prevSearches, search]
                        }
                    }

                }
            )
        }
    }


    render() {
        return (
            <div className="App">
                <h1 className="mt-2">Github Repository Search</h1>
                <Container fluid className="d-flex flex-row justify-content-center">
                    <div className="col-9">
                        <Search
                            handleInputChange={this.handleInputChange}
                            minStars={this.state.minStars}
                            maxStars={this.state.maxStars}
                            dateFrom={this.state.dateFrom}
                            dateTo={this.state.dateTo}
                            options={this.state.options}
                            multiselectRef={this.multiselectRef}
                            onSubmit={this.onSubmitSearch}
                        />

                        {
                            (this.state.repositories.length > 0)
                                ?
                                <RepositoryBlock
                                    data={this.state.repositories}
                                    requestDetails={this.requestDetails}
                                />
                                :
                                <h4 className="mt-2">
                                    Go ahead and click on search button
                                </h4>
                        }


                        {this.state.repositories.length > 0 &&
                        <div className="d-flex align-self-center">
                            <Pagination
                                lastPage={this.state.lastPage}
                                changePage={this.onSubmitSearch}
                            />
                        </div>
                        }

                    </div>
                    <div className="col-3">
                        <PreviousSearches
                            searches={this.state.prevSearches}
                            onSubmit={this.onSubmitSearch}
                        />
                    </div>
                </Container>

            </div>
        );
    }


}

export default App;
