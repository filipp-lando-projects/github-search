import {Card, ListGroup} from "react-bootstrap"
import React from "react";

const PreviousSearches = (props) => {

    return (
        <div>
            {
                <Card>
                    <Card.Header>Previous Search</Card.Header>
                    <ListGroup variant="flush">
                        {
                            props.searches.map((s, i) => {
                                return (
                                    <a href="#" key={"search-" + i} onClick={() => {
                                        props.onSubmit(s)
                                    }}>
                                        {s.date_from}..{s.date_to}, lang:{s.programming_languages.join("|")}
                                    </a>
                                )
                            })

                        }
                    </ListGroup>
                </Card>
            }
        </div>
    )

}

export default PreviousSearches;