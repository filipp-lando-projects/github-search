import React, {useState} from "react"
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import {MDBBtn, MDBCol, MDBInput, MDBRow} from 'mdb-react-ui-kit';
import Multiselect from 'multiselect-react-dropdown';
import {KeyboardDatePicker, MuiPickersUtilsProvider} from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import moment from "moment";

const Search = (props) => {

    const [dateFrom, setDateFrom] = useState(moment.unix(0))
    const [dateTo, setDateTo] = useState(moment.now())

    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <Navbar bg="light" expand="lg" sticky='top'>
                <div className="container">
                    <MDBRow className="d-flex align-items-start justify-content-between">
                        <MDBCol>
                            <MDBInput
                                type="number"
                                id='min-stars-input'
                                label='Min Stars'
                                onChange={(e) => props.handleInputChange(e, "minStars")}
                                value={props.minStars}
                            />
                            <MDBInput
                                className="mt-2"
                                type="number"
                                id='max-stars-input'
                                label='Max Stars'
                                onChange={(e) => props.handleInputChange(e, "maxStars")}
                                value={props.maxStars}
                            />
                        </MDBCol>
                        <MDBCol>
                            <Multiselect
                                options={props.options} // Options to display in the dropdown
                                displayValue="name" // Property name to display in the dropdown options
                                showCheckbox={true}
                                hidePlaceholder={true}
                                ref={props.multiselectRef}
                            />
                        </MDBCol>
                        <MDBCol>
                            <KeyboardDatePicker
                                label="Updated From"
                                clearable
                                value={props.dateFrom}
                                onChange={() => {
                                }}
                                format="DD/MM/yyyy"
                                disableFuture={true}
                            />
                            <KeyboardDatePicker
                                className="mt-2"
                                label="Updated To"
                                clearable
                                value={props.dateTo}
                                minDate={props.dateFrom}
                                minDateMessage="Date should not be before minimal date"
                                onChange={() => {
                                }}
                                format="DD/MM/yyyy"
                                disableFuture={true}
                            />
                        </MDBCol>

                        <MDBCol className="align-self-center">
                            <MDBBtn onClick={()=>{props.onSubmit()}} color="primary">Search</MDBBtn>
                        </MDBCol>
                    </MDBRow>

                </div>
            </Navbar>
        </MuiPickersUtilsProvider>
    )
}

export default Search;