import {MDBPaginationItem, MDBPaginationLink} from "mdb-react-ui-kit";
import React, {useState} from "react";


const Pagination = (props) => {
    const [pageNumbers, setPageNumbers] = useState([1, 2, 3])
    const [currentPage, setCurrentPage] = useState(1)

    const reducePagination = () => {
        if (pageNumbers[0] > 1) {
            setPageNumbers(pageNumbers.map(el => el - 1))
            props.changePage(null, currentPage - 1)
            setCurrentPage(currentPage - 1)
        }
    }

    const increasePagination = () => {
        if (pageNumbers[pageNumbers.length - 1] < props.lastPage) {
            setPageNumbers(pageNumbers.map(el => el + 1))
            props.changePage(null, currentPage + 1)
            setCurrentPage(currentPage + 1)
        }
    }

    const localChangePage = (num) => {
        props.changePage(null,num);
        setCurrentPage(num);
    }

    return (
        <nav aria-label='Page navigation example'>
            <ul className='pagination mb-0'>
                <MDBPaginationItem>
                    <MDBPaginationLink href='#'
                                       aria-label='Previous'
                                       onClick={reducePagination}
                    >
                        <span aria-hidden='true'>«</span>
                    </MDBPaginationLink>
                </MDBPaginationItem>
                {
                    pageNumbers.map(((num, i) => {
                        return (
                            <MDBPaginationItem
                                key={"pagination-item-" + i}

                            >
                                <MDBPaginationLink
                                    key={"pagination-number-" + i}
                                    href="#"
                                    onClick={() => {
                                        localChangePage(num)
                                    }}
                                >
                                    {num}
                                </MDBPaginationLink>
                            </MDBPaginationItem>
                        )
                    }))
                }
                <MDBPaginationItem>
                    <MDBPaginationLink href='#'
                                       aria-label='Next'
                                       onClick={increasePagination}
                    >
                        <span aria-hidden='true'>»</span>
                    </MDBPaginationLink>
                </MDBPaginationItem>
            </ul>
        </nav>
    );
}

export default Pagination;