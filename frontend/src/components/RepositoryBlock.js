import {MDBAccordion, MDBAccordionItem, MDBBadge, MDBIcon} from "mdb-react-ui-kit";
import React, {useState} from "react";
import moment from "moment";
import {getPrsAndIssues} from "../api/api";
import Accordion from 'react-bootstrap/Accordion'


const RepositoryBlock = (props) => {

    const [items, setItems] = useState({})

    const requestDetails = async (user, repository) => {
        if (repository in items) {
            return items[repository];
        } else {
            getPrsAndIssues({user: user, repository: repository}).then(response => {
                setItems({
                    ...items, [repository]: {
                        total_count_prs: response.total_count_prs,
                        total_count_open_issues: response.total_count_open_issues,
                        total_count_closed_issues: response.total_count_closed_issues
                    }
                })
            })
        }

    }

    return (
        <div>
            <Accordion>
                {
                    props.data.map((el, id) => {
                        return (
                            <Accordion.Item
                                key={id}
                                eventKey={id}
                                onClick={() => {
                                    requestDetails(el.user, el.name)
                                }}

                            >
                                <Accordion.Header>
                                    {el.name}
                                </Accordion.Header>
                                <Accordion.Body>

                                    {
                                        (el.name in items)
                                            ?
                                            <div>
                                                <div
                                                    className="d-flex flex-row align-items-start justify-content-between">
                                                    <div>
                                                        {el.description}
                                                    </div>
                                                    <div>
                                                        <MDBIcon far icon="star"/> {el.stars}
                                                        <MDBBadge className="m-2"
                                                                  color='success'>{el.language}</MDBBadge>
                                                    </div>
                                                </div>
                                                <div className="d-flex flex-row align-items-start justify-content-between">
                                                    <div>
                                                        <a href={el.url}>{el.url}</a>
                                                    </div>
                                                    <div>
                                                        PRs {items[el.name].total_count_prs}
                                                    </div>
                                                </div>

                                                <div className="d-flex flex-row align-items-start justify-content-between">
                                                    <div >
                                                        Updated: {moment.utc(el.updated).format("DD-MM-YYYY")}
                                                    </div>
                                                    <div>
                                                        Open Issues: {items[el.name].total_count_open_issues}
                                                    </div>
                                                </div>
                                                <div className="d-flex flex-row align-items-start justify-content-between">
                                                    <div >
                                                        Created: {moment.utc(el.created).format("DD-MM-YYYY")}
                                                    </div>
                                                    <div>
                                                        Closed Issues: {items[el.name].total_count_closed_issues}
                                                    </div>
                                                </div>

                                            </div>
                                            :
                                            <div>
                                                <div className="spinner-border" role="status">
                                                    <span className="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                    }
                                </Accordion.Body>

                            </Accordion.Item>
                        )
                    })

                }
            </Accordion>
        </div>
    )
}

export default RepositoryBlock