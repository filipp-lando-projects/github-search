const url = process.env.REACT_APP_API_URL + 'api/';

export const GET_LANGUAGES_LIST = url + "languages/";
export const GET_REPOSITORIES = url + "repositories/";
export const GET_PRS_ISSUES = url + "prs_issues/";