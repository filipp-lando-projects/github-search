import axios from "axios";
import * as urls from "./urls"


const makeRequest = async (url, error_message, request = null,) => {
    let response;
    try {
        response = request ? await axios.post(url, request) : await axios.get(url)
        if (response.data) {
            return response.data;
        } else {
            return []
        }
    } catch (err) {
        console.log(error_message);
        throw err;
    }
}
export const getLanguagesList = async () => {
    return await makeRequest(urls.GET_LANGUAGES_LIST, "Error while getting language list")
}

export const getRepositories = async (request) => {
    return await makeRequest(urls.GET_REPOSITORIES, "Error while getting repositories info", request)
}

export const getPrsAndIssues = async (request) => {
    return await makeRequest(
        urls.GET_PRS_ISSUES,
        "Error while getting pull requests and issues info",
        request)
}