Django==3.1
djangorestframework==3.11.1
requests==2.24.0
distlib==0.3.3
django-cors-headers==3.4.0
