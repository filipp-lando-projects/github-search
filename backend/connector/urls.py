from django.urls import path

from . import views

urlpatterns = [
    path('repositories/', views.get_repositories, name='get_repositories'),
    path('languages/', views.get_languages, name='get_languages'),
    path('prs_issues/', views.get_open_pull_requests_issues, name='get_prs_issues')
]
