import json

import requests
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

languages_list = [
    "Java",
    "C#",
    "PHP",
    "CSS",
    "Ruby",
    "Lua",
    "Python",
    "JavaScript",
    "Apex",
    "HTML",
    "ASP",
    "C",
    "SQL",
    "Go",
    "PowerShell",
    "Fortran",
    "Kobol",
    "Bash",
    "HTML",
    "CSS"
]

BASE_URL = "https://api.github.com/"
HEADERS = {"Accept": "application/vnd.github.v3+json"}


def repository_query_builder(params, base_url):
    min_stars = str(params.data['min_stars']) if ('min_stars' in params.data) else None
    max_stars = str(params.data['max_stars']) if ('max_stars' in params.data) else None
    page_number = str(params.data['page_number']) if 'page_number' in params.data else None
    date_from = str(params.data['date_from']) if 'date_from' in params.data else None
    date_to = str(params.data['date_to']) if 'date_to' in params.data else None
    programming_languages = params.data['programming_languages'] if 'programming_languages' in params.data else None

    stars = ""
    if min_stars is not None and max_stars is not None:
        stars = "stars:" + min_stars + ".." + max_stars
    elif min_stars is not None:
        stars = "stars:>" + min_stars
    elif max_stars is not None:
        stars = "stars:<" + max_stars
    elif min_stars is None and max_stars is None:
        stars = "stars:>" + str(0)

    languages = ""
    request = base_url + "search/repositories?q=pushed:" + date_from + ".." + date_to + " "
    if len(programming_languages) != 0:
        languages += "language:\"" + "\"+language:\"".join(programming_languages) + "\" "
        request += languages + stars
    if page_number is not None:
        request += "&page=" + page_number
    return request


def pr_issue_query_builder(params, base_url):
    repository = str(params.data['repository']) if ('repository' in params.data) else None
    user = str(params.data['user']) if ('user' in params.data) else None
    if repository is None or user is None:
        raise Exception("All parameters should be declared")
    return {"prs_request": base_url + "search/issues?q=repo:" + user + "/" + repository + " is:pr is:open",
            "issues_requests_open": base_url + "search/issues?q=repo:" + user + "/" + repository + " is:issue is:open",
            "issues_requests_closed": base_url + "search/issues?q=repo:" + user + "/" + repository + " is:issue is:closed"}


@api_view(["POST"])
def get_repositories(request):
    query = repository_query_builder(request, BASE_URL)
    github_response = requests.get(query, headers=HEADERS)

    if github_response.status_code == 200:
        link = github_response.headers['Link']
        last_page = list(filter(lambda x: "last" in x, link.split("page=")))[0].split(">")[0]
        my_response = {
            "last_page": last_page,
            "repositories": []
        }
        # parse github api response
        github_response = json.loads(github_response.text)
        for item in github_response['items']:
            my_response['repositories'].append({
                "name": item['name'],
                "description": item['description'],
                "created": item['created_at'],
                "updated": item['updated_at'],
                "open_issues": item['open_issues'],
                "language": item['language'],
                "stars": item['stargazers_count'],
                "url": item['html_url'],
                "languages_url": item['languages_url'],
                "user": item['owner']['login']
            })

        return Response(my_response, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["POST"])
def get_open_pull_requests_issues(request):
    queries = pr_issue_query_builder(request, BASE_URL)
    github_response_prs = requests.get(queries["prs_request"], headers=HEADERS)
    github_response_open_issues = requests.get(queries["issues_requests_open"], headers=HEADERS)
    github_response_closed_issues = requests.get(queries["issues_requests_closed"], headers=HEADERS)
    if github_response_prs.status_code == 200 and github_response_open_issues.status_code == 200:
        response = {
            'total_count_prs': json.loads(github_response_prs.text)['total_count'],
            'total_count_open_issues': json.loads(github_response_open_issues.text)['total_count'],
            'total_count_closed_issues': json.loads(github_response_closed_issues.text)['total_count'],

        }
        return Response(response, status=status.HTTP_200_OK)
    else:
        return Response({'total_count_prs': 10,
                         'total_count_open_issues': 20,
                         'total_count_closed_issues': 30}, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(["GET"])
def get_languages(request):
    return Response(languages_list, status=status.HTTP_200_OK)
